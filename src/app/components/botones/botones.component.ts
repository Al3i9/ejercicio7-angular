import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-botones',
  templateUrl: './botones.component.html',
  styleUrls: ['./botones.component.css']
})
export class BotonesComponent implements OnInit {
  opcion=0;

  mostrar:boolean=true;
  valornumerico:any=3;


  constructor() { }
  ngOnInit(): void {
  }

  caso1(){
    this.opcion=1;
   }
   caso2(){
    this.opcion=2;
   }
   caso3(){
    this.opcion=3;
   }

}
